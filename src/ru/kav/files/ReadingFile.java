package ru.kav.files;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
/**
 * Created by kav on 23.03.2017.
 */
public class ReadingFile {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("pap//input.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("pap//out.txt"));


        String string;
        while ((string = bufferedReader.readLine()) != null) {
            String[] strings = string.split(" ");
            int num1 = Integer.valueOf(strings[0]);
            int num2 = Integer.valueOf(strings[2]);
            char sym = strings[1].charAt(0);
            int res = calculation(num1, num2, sym);
            System.out.println(res);
            bufferedWriter.write(res + "\n");
            bufferedWriter.flush();



        }
        bufferedReader.close();
    }

    private static int calculation(int num1, int num2, char sym) {
        switch (sym) {
            case '+':
                return num1 + num2;
            case '-':
                return num1 - num2;
            case '*':
                return num1 * num2;
        }
        return 0;
    }
}

